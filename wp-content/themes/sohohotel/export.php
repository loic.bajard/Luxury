<?php

	/*
		Template Name: Export CSV
	*/

	if ( is_user_logged_in() ) {

		// Init

		$bdd = 'mysql:host=localhost;dbname=logomoti_luxury';
		$usr = 'logomoti_luxury';
		$pwd = '68Mtu7EXGMND';

		// Connection

		$pdo = new PDO($bdd, $usr, $pwd, array(
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));

		// Query

		$query = "SELECT post_id as Id,
		    MAX(IF(`meta_key` = 'quitenicebooking_guest_first_name', meta_value, NULL)) 'FirstName',
		    MAX(IF(`meta_key` = 'quitenicebooking_guest_last_name', meta_value, NULL)) 'LastName',
		    MAX(IF(`meta_key` = 'quitenicebooking_guest_details', meta_value, NULL)) 'Details',
		    MAX(IF(`meta_key` = 'quitenicebooking_guest_email', meta_value, NULL)) 'Email'
		FROM wp_postmeta
		GROUP BY Id";

		// Getting

		$stmt = $pdo->query($query);

		$list = array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			if ($row['Email'] !== NULL) {
				$insert = true;
				foreach ($list as $key => $value) {
					if($row['Email'] == $value['Email']) {
						$insert = false;
						break;
					}
				}
				if($insert) {
					$details = json_decode($row['Details']);
					$list[] = $arrayName = array(
						'FirstName' => $row['FirstName'],
						'LastName' => $row['LastName'],
						'Address' => $details->address->value,
						'Zip' => $details->zip->value,
						'City' => $details->city->value,
						'Country' => $details->country->value,
						'Phone' => $details->phone->value,
						'Email' => $row['Email']
					);
				}
			}
		}

		// Output

		$fp = fopen('php://output', 'w');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="export.csv"');
		foreach ($list as $ferow) {
		    fputcsv($fp, $ferow);
		}

	}